package com.truevolve.android.enact.controllers.samplelocation;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.truevolve.android.enact.controllers.getlocation.LocationWrapper;
import com.truevolve.enact.DataStore;
import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.ObjectNotFoundException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

public class Summary extends ActivityBaseController {
    private static final String TAG = "Summary",
            ON_DONE = "on_done",
            SAVED_LOCATION = "saved_location",
            DISPLAY_MESSAGE = "display_message";
    private static final String CONTROLLER_TYPE = "summary";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        final Interpreter interp = Interpreter.INSTANCE;

        DataStore data = interp.getDataStore();

        TextView longitude = (TextView) findViewById(R.id.longitudeText);
        TextView latitude = (TextView) findViewById(R.id.latitudeText);
        TextView message = (TextView) findViewById(R.id.messageText);

        Button done = (Button) findViewById(R.id.doneButton);

        try {
            LocationWrapper location = data.getProperty(getStateObj().getString(SAVED_LOCATION));

            message.setText(getStateObj().getString(DISPLAY_MESSAGE));
            latitude.setText(Double.toString(location.getLatitude()));
            longitude.setText(Double.toString(location.getLongitude()));

            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        goToState(getStateObj().getString(ON_DONE));
                    } catch (InterpreterException | JSONException e) {
                        e.printStackTrace();
                        interp.error(Summary.this, e);
                    }
                }
            });

        } catch (JSONException | InvocationTargetException | IllegalAccessException | ObjectNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
            interp.error(this, e);
        }

    }

    @Override
    public String getType() {
        return CONTROLLER_TYPE;
    }

    @Override
    public void validate(JSONObject stateObj) throws PolicyException {
        Log.d(TAG, "validate: starting");

        if (!stateObj.has(ON_DONE)) {
            Log.e(TAG, "validate: state object must have " + ON_DONE + " defined");
            throw new PolicyException("State object must have " + ON_DONE + " defined");

        } else if (!stateObj.has(SAVED_LOCATION)) {
            Log.e(TAG, "validate: state object must have " + SAVED_LOCATION + " defined");
            throw new PolicyException("State object must have " + SAVED_LOCATION + " defined");

        } else if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e(TAG, "validate: state object must have " + DISPLAY_MESSAGE + " defined");
            throw new PolicyException("State object must have " + DISPLAY_MESSAGE + " defined");
        }
    }
}

package com.truevolve.android.enact.controllers.samplelocation;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;
import org.json.JSONObject;

public class Menu extends ActivityBaseController {

    private static final String ON_FIND = "on_find";
    private static final String TAG = "Menu";
    private static final String CONTROLLER_TYPE = "menu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        try {
            final String onFind = getStateObj().getString(ON_FIND);
            findViewById(R.id.findMeButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        goToState(onFind);
                    } catch (InterpreterException | JSONException e) {
                        e.printStackTrace();
                        Interpreter.INSTANCE.error(Menu.this, e);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getType() {
        return CONTROLLER_TYPE;
    }

    @Override
    public void validate(JSONObject stateObj) throws PolicyException {
        if (!stateObj.has(ON_FIND)) {
            Log.e(TAG, "validate: state object does not have " + ON_FIND + " which is mandatory");
            throw new PolicyException("State object does not have " + ON_FIND + " which is mandatory");
        }
    }
}

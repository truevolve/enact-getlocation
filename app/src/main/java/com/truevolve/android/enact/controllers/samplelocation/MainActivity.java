package com.truevolve.android.enact.controllers.samplelocation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.truevolve.android.enact.controllers.getlocation.GetLocation;
import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private String policy =
            "{\n" +
                    "  \"start\": {\n" +
                    "    \"type\": \"menu\",\n" +
                    "    \"on_find\": \"find_my_location\"\n" +
                    "  },\n" +
                    "  \"find_my_location\": {\n" +
                    "    \"type\": \"get_location\",\n" +
                    "    \"store_as\": \"location_deets\",\n" +
                    "    \"on_next\":\"summary\",\n" +
                    "    \"display_message\": \"Getting your location, please be patient...\"\n" +
                    "  },\n" +
                    "  \"summary\": {\n" +
                    "    \"type\": \"summary\",\n" +
                    "    \"on_done\": \"end\",\n" +
                    "    \"saved_location\":\"location_deets\",\n" +
                    "    \"display_message\": \"Your location is: \"\n" +
                    "  }\n" +
                    "}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent().hasExtra(Interpreter.END)) {
            Log.d(TAG, "onCreate: ENDED");
        }
        if (getIntent().hasExtra(Interpreter.ERROR)) {
            Log.d(TAG, "onCreate: ERRORED");
            Throwable throwable = (Throwable) getIntent().getSerializableExtra("exception");
            Log.e(TAG, "onCreate: Errored: ", throwable);
        }

        List<Class<? extends ActivityBaseController>> listOfControllers = new ArrayList<>();
        listOfControllers.add(Menu.class);
        listOfControllers.add(GetLocation.class);
        listOfControllers.add(Summary.class);

        try {
            Interpreter interpreter = Interpreter.INSTANCE.setup(MainActivity.class, policy, listOfControllers);
            if (interpreter != null) {
                interpreter.start(this);
            }

        } catch (JSONException | PolicyException | InterpreterException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: RESUMING");
    }
}

package com.truevolve.android.enact.controllers.getlocation;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;
import org.json.JSONObject;

public class GetLocation extends ActivityBaseController implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "GetLocation",
            STORE_AS = "store_as",
            ON_NEXT = "on_next",
            DISPLAY_MESSAGE = "display_message";
    private static final int REQUEST_LOCATION = 321;
    private static final String CONTROLLER_TYPE = "get_location";

    private String storeAs, gotLocation, displayMessage;

    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_location);

        try {
            storeAs = getStateObj().getString(STORE_AS);
            gotLocation = getStateObj().getString(ON_NEXT);
            displayMessage = getStateObj().getString(DISPLAY_MESSAGE);

            TextView text = (TextView) findViewById(R.id.messageText);
            text.setText(displayMessage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create an instance of GoogleAPIClient.
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected: starting");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            Log.i(TAG, "onConnected: Permission for location not granted, requesting now.");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

            return;
        }

        Log.i(TAG, "onConnected: Permission for location already granted");

        LocationWrapper location = new LocationWrapper(LocationServices.FusedLocationApi.getLastLocation(googleApiClient));
        Interpreter.INSTANCE.getDataStore().put(storeAs, location);

        try {
            goToState(gotLocation);
        } catch (InterpreterException | JSONException e) {
            e.printStackTrace();
            Interpreter.INSTANCE.error(this, e);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_LOCATION) {
            Log.i(TAG, "Received response for location permission request.");

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Location permission has now been granted. Requesting location.");

                //here we need to ask for a connect, because we have just gotten permission.
                googleApiClient.reconnect();

            } else {
                Log.i(TAG, "Location permission was NOT granted.");

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: starting");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: starting");
    }

    @Override
    public String getType() {
        return CONTROLLER_TYPE;
    }

    @Override
    public void validate(JSONObject stateObj) throws PolicyException {
        Log.d(TAG, "validate: starting");

        if (!stateObj.has(STORE_AS)) {
            Log.e(TAG, "validate: state object must have " + STORE_AS + " defined");
            throw new PolicyException("State object must have " + STORE_AS + " defined");

        } else if (!stateObj.has(ON_NEXT)) {
            Log.e(TAG, "validate: state object must have " + ON_NEXT + " defined");
            throw new PolicyException("State object must have " + ON_NEXT + " defined");

        } else if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e(TAG, "validate: state object must have " + DISPLAY_MESSAGE + " defined");
            throw new PolicyException("State object must have " + DISPLAY_MESSAGE + " defined");
        }
    }
}

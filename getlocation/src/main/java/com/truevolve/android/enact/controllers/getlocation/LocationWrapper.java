package com.truevolve.android.enact.controllers.getlocation;

import android.location.Location;

/**
 * Created by Steyn Geldenhuys on 3/21/17.
 */

public class LocationWrapper {

    private float accuracy;
    private double altitude;
    private float bearing;
    private double latitude;
    private double longitude;
    private String provider;
    private float speed;
    private long time;
    private String type = "location";

    public LocationWrapper() {
    }

    public LocationWrapper(Location location) {
        if (location != null) {
            accuracy = location.getAccuracy();
            altitude = location.getAltitude();
            bearing = location.getBearing();
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            provider = location.getProvider();
            speed = location.getSpeed();
            time = location.getTime();
        }
    }

    public float getAccuracy() {
        return accuracy;
    }

    public double getAltitude() {
        return altitude;
    }

    public float getBearing() {
        return bearing;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getProvider() {
        return provider;
    }

    public float getSpeed() {
        return speed;
    }

    public long getTime() {
        return time;
    }

    public String getType() {
        return type;
    }
}
